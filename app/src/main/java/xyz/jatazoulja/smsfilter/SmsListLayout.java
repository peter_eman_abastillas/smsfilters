package xyz.jatazoulja.smsfilter;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class SmsListLayout extends LinearLayout {
    public SmsListLayout(Context context) {
        super(context);
    }

    public SmsListLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmsListLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SmsListLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
